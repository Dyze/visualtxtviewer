﻿namespace VisualTxtViwer.Extensions
{
    public static class StringNumberExtension
    {
        public static int ParseInt(this StringNumber number)
        {
            // parses the StringNumbers to actual Numbers
            switch (number.Number)
            {
                case @"---/\--":
                    return 3;
                case @"---_||---":
                    return 2;
                case @"||||":
                    return 1;
                case @"|||___|||":
                    return 4;
                case @"-----|___|____|":
                    return 5;
                default:
                    return -1;
            }
        }
    }
}
