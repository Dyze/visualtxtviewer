﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace VisualTxtViwer.WindowBase
{
    public class PropertyHandler : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string name = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
