﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VisualTxtViwer.Extensions;
using VisualTxtViwer.WindowBase;

namespace VisualTxtViwer
{
    class MainWindowViewModel : PropertyHandler
    {
        private string _windowInput;
        private string filePath;

        public string WindowInput
        {
            get { return _windowInput; }
            set
            {
                _windowInput = value;
                NotifyPropertyChanged();
            }
        }

        public string FilePath 
        {
            get { return filePath; }
            set 
            {
                filePath = value;
                ConvertStringNumbers();
            }
        }

        public int NumberCount { get; set; }

        public void ConvertStringNumbers()
        {
            string fileOutput = readFileText(FilePath);
            if (string.IsNullOrEmpty(fileOutput))
                return;

            var list = getStringNumbers(fileOutput);

            string numbers = string.Empty;
            foreach (var item in list)
            {
                numbers += $"{item.ParseInt()}, ";
            }

            //removes the last , (1,2, => 1,2)
            numbers = numbers.Remove(numbers.LastIndexOf(","), numbers.Length - numbers.LastIndexOf(","));
            WindowInput = numbers;
        }

        //separtes every StringNumber from the rest
        private List<StringNumber> getStringNumbers(string fileOutput)
        {
            //before everything remove tabs, newlines, and trims the start also removes the spacing between |   | in the 4
            fileOutput = fileOutput.TrimStart().Replace("\t", " ").Replace(Environment.NewLine, " NewLine ").Replace("|   |", "||");

            List<StringNumber> numberList = new List<StringNumber>();
            int row = 0;
            int col = 0;
            int rowReset = 0;
            // a number is always 4 rows big after 4 rows there can be a new one (rowReset * 4 for current row)
            for (int i = 0; i < int.MaxValue; i++)
            {
                // if its the first row add the StringNumber to a list 
                // else add the new string to the StringNumber
                if (row <= 0)
                {
                    fileOutput = fileOutput.TrimStart();

                    string stringNumberStart = fileOutput.Substring(0, fileOutput.IndexOf(" "));
                    numberList.Add(new StringNumber(stringNumberStart, col, 4 * rowReset));
                    fileOutput = fileOutput.Remove(0, fileOutput.IndexOf(" ")).TrimStart();

                    col++;
                    row = fileOutput.StartsWith("NewLine") ? row + 1 : row;
                }
                else
                {
                    if (fileOutput.StartsWith("NewLine"))
                    {
                        fileOutput = fileOutput.Remove(0, fileOutput.IndexOf(" ")).TrimStart();
                        col = 0;
                        row++;

                        if (row > 4)
                        {
                            row = 0;
                            rowReset++;
                            continue;
                        }
                    }

                    string newNumber = string.Empty;
                    //get the old StringNumber and add the new string
                    StringNumber oldNumber = numberList.Where(x => x.Column == col && x.Row == (4 * rowReset)).Single();

                    if (fileOutput.IndexOf(" ") <= 0)
                    {
                        newNumber = fileOutput.Trim();
                        oldNumber.Number = $"{oldNumber.Number}{newNumber}";
                        break;
                    }
                    else
                    {
                        newNumber = fileOutput.Substring(0, fileOutput.IndexOf(" "));

                        oldNumber.Number = $"{oldNumber.Number}{newNumber}";
                        fileOutput = fileOutput.Remove(0, fileOutput.IndexOf(" ")).TrimStart();

                        col++;
                    }
                }
            }

            return numberList;
        }

        // opens a file and returns you all the text in it
        private string readFileText(string filePath)
        {
            if(File.Exists(filePath))
                return File.ReadAllText(filePath);
            return null;
        }
    }
}
