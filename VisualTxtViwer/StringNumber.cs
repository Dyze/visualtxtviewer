﻿namespace VisualTxtViwer
{
    public class StringNumber
    {
        public StringNumber(string number, int column, int row)
        {
            Number = number;
            Column = column;
            Row = row;
        }

        public string Number { get; set; }
        public int Column { get; set; }
        public int Row { get; set; }
    }
}
